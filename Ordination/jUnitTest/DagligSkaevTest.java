package jUnitTest;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligSkaev;
import ordination.Dosis;
import ordination.Laegemiddel;
import ordination.Patient;

public class DagligSkaevTest {
	private Patient p1;
	private Laegemiddel l1;
	private DagligSkaev basisData1;

	@Before
	public void setUp() throws Exception {
		p1 = new Patient("121256-0512", "Jane Jensen", 63.4);
		l1 = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "styk");
		basisData1 = new DagligSkaev(LocalDate.of(2021, 03, 01), LocalDate.of(2021, 03, 10), l1);
	}

	// ------------------opretDosis()

	@Test
	public void opretDosisTC1() {
		Dosis d = new Dosis(LocalTime.of(9, 30), 2);

		int size = basisData1.getDoser().size();
		basisData1.opretDosis(LocalTime.of(9, 30), 2);

		assertEquals(d, basisData1.getDoser().get(size));
	}

	// -----------------getDoser()

	@Test
	public void getDoserTC1() {

		Dosis d1 = new Dosis(LocalTime.of(9, 30), 2);
		Dosis d2 = new Dosis(LocalTime.of(12, 20), 1);
		Dosis d3 = new Dosis(LocalTime.of(15, 10), 1);

		int size = basisData1.getDoser().size();

		basisData1.opretDosis(LocalTime.of(9, 30), 2);
		basisData1.opretDosis(LocalTime.of(12, 20), 1);
		basisData1.opretDosis(LocalTime.of(15, 10), 1);

		assertEquals(d1, basisData1.getDoser().get(size + 0));
		assertEquals(d2, basisData1.getDoser().get(size + 1));
		assertEquals(d3, basisData1.getDoser().get(size + 2));

	}

	// -----------------samletDosis()

	@Test
	public void samletDosisTC1() {
		basisData1.opretDosis(LocalTime.of(9, 30), 2);
		basisData1.opretDosis(LocalTime.of(12, 20), 1);
		basisData1.opretDosis(LocalTime.of(15, 10), 1);

		assertEquals(4, basisData1.samletDosis(), 0.001);
	}

	// --------------doegnDosis()

	@Test
	public void doegnDosisTC1() {
		basisData1.opretDosis(LocalTime.of(9, 30), 2);
		basisData1.opretDosis(LocalTime.of(12, 20), 1);
		basisData1.opretDosis(LocalTime.of(15, 10), 1);

		assertEquals(0.4, basisData1.doegnDosis(), 0.001);
	}

}
