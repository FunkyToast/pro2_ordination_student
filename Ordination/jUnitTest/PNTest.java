package jUnitTest;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class PNTest {
	private Patient p1;
	private Laegemiddel l1;
	private PN basisData1, basisData2;

	@Before
	public void setUp() throws Exception {
		p1 = new Patient("121256-0512", "Jane Jensen", 63.4);
		l1 = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "styk");
		basisData1 = new PN(LocalDate.of(2021, 03, 01), LocalDate.of(2021, 03, 10), l1, 2);
		basisData2 = new PN(LocalDate.of(2021, 03, 01), LocalDate.of(2021, 03, 10), l1, 2);
		basisData1.givDosis(LocalDate.of(2021, 03, 1));
		basisData1.givDosis(LocalDate.of(2021, 03, 2));
		basisData1.givDosis(LocalDate.of(2021, 03, 4));
		basisData1.givDosis(LocalDate.of(2021, 03, 6));
		basisData1.givDosis(LocalDate.of(2021, 03, 8));

	}

	// -----------givDosis Test
	// -----------TC1:null
	@Test(expected = NullPointerException.class)
	public void givDosisTC1() {
		basisData1.givDosis(null);

	}

	@Test
	public void givDosisTC2() {
		boolean res = basisData1.givDosis(LocalDate.of(2021, 02, 01));
		assertEquals(false, res);

	}

	@Test
	public void givDosisTC3() {
		boolean res = basisData1.givDosis(LocalDate.of(2021, 04, 01));
		assertEquals(false, res);

	}

	@Test
	public void givDosisTC4() {
		boolean res = basisData1.givDosis(LocalDate.of(2021, 03, 05));
		assertEquals(true, res);

	}

	@Test
	public void givDosisTC5() {
		boolean res = basisData1.givDosis(LocalDate.of(2021, 03, 01));
		assertEquals(true, res);

	}

	@Test
	public void givDosisTC6() {
		boolean res = basisData1.givDosis(LocalDate.of(2021, 03, 10));
		assertEquals(true, res);

	}

	// ------doegnDosis()

	@Test
	public void doegnDosisTC1() {
		double res = basisData1.doegnDosis();
		assertEquals(1.25, res, 0.001);

	}

	@Test
	public void doegnDosisTC2() {
		double res = basisData2.doegnDosis();
		assertEquals(0, res, 0.001);

	}

	// --------------samletDosis

	@Test
	public void samletDosisTC1() {
		double res = basisData1.samletDosis();
		assertEquals(10, res, 0.001);

	}

	@Test
	public void samletDosisTC2() {
		double res = basisData2.samletDosis();
		assertEquals(0, res, 0.001);

	}

	// -----------------getAntalGangeGivet()

	@Test
	public void getAntalGangeGivetTC1() {
		int res = basisData1.getAntalGangeGivet();
		assertEquals(5, res);

	}

	@Test
	public void getAntalGangeGivetTC2() {
		int res = basisData2.getAntalGangeGivet();
		assertEquals(0, res);

	}

	// -----------getAntalEnheder()

	@Test
	public void getAntalEnhederTC1() {
		double res = basisData2.getAntalEnheder();
		assertEquals(2, res, 0.001);

	}

}
