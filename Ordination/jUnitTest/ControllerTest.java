package jUnitTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import controller.Controller;
import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class ControllerTest {
	Controller controller;
	Patient p1, p2, p3, p4, p5;
	Laegemiddel l1;

	@Before
	public void setUp() throws Exception {
		controller = Controller.getController();
		p1 = controller.opretPatient("121256-0512", "Jane Jensen", 63.4);
		l1 = controller.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");

		p2 = controller.opretPatient("032149-2558", "Anna Stensen", 20);
		p3 = controller.opretPatient("070985-1153", "Finn Madsen", 83.2);
		p4 = controller.opretPatient("062149-2528", "Troels Axelsen", 130);
		p5 = controller.opretPatient("121287-0967", "Tom Hansen", 70);
	}

	// -------opretPNOrdination();

	@Test
	public void opretPNOrdinationTC1() {
		int size = p1.getOrdinationer().size();

		LocalDate start = LocalDate.of(2021, 02, 01);
		PN pn = controller.opretPNOrdination(start, LocalDate.of(2021, 02, 10), p1, l1, 2);

		assertEquals(pn, p1.getOrdinationer().get(size));
	}

	@Test(expected = IllegalArgumentException.class)
	public void opretPNOrdinationTC2() {
		int size = p1.getOrdinationer().size();

		LocalDate start = LocalDate.of(2021, 02, 01);
		PN pn = controller.opretPNOrdination(start, LocalDate.of(2021, 1, 1), p1, l1, 2);

		assertEquals(pn, p1.getOrdinationer().get(size));
	}

	@Test(expected = IllegalArgumentException.class)
	public void opretPNOrdinationTC3() {
		int size = p1.getOrdinationer().size();

		LocalDate start = LocalDate.of(2021, 02, 01);
		PN pn = controller.opretPNOrdination(start, LocalDate.of(2021, 2, 1), p1, l1, 2);

		assertEquals(pn, p1.getOrdinationer().get(size));
	}

	// -----------opretDagligFastOrdination()

	@Test
	public void opretDagligFastOrdinationTC1() {
		int size = p1.getOrdinationer().size();

		LocalDate start = LocalDate.of(2021, 02, 01);
		DagligFast df = controller.opretDagligFastOrdination(start, LocalDate.of(2021, 2, 10), p1, l1, 1, 1, 1, 1);

		assertEquals(df, p1.getOrdinationer().get(size));
	}

	@Test(expected = IllegalArgumentException.class)
	public void opretDagligFastOrdinationTC2() {
		int size = p1.getOrdinationer().size();

		LocalDate start = LocalDate.of(2021, 02, 01);
		DagligFast df = controller.opretDagligFastOrdination(start, LocalDate.of(2021, 1, 1), p1, l1, 2, 2, 2, 2);

		assertEquals(df, p1.getOrdinationer().get(size));
	}

	@Test(expected = IllegalArgumentException.class)
	public void opretDagligFastOrdinationTC3() {
		int size = p1.getOrdinationer().size();

		LocalDate start = LocalDate.of(2021, 02, 01);
		DagligFast df = controller.opretDagligFastOrdination(start, LocalDate.of(2021, 2, 1), p1, l1, 2, 2, 2, 2);

		assertEquals(df, p1.getOrdinationer().get(size));
	}

	// -----------opretDagligskaevOrdination()

	@Test
	public void opretDagligskaevOrdinationTC1() {
		LocalTime[] lt = { LocalTime.of(8, 00), LocalTime.of(16, 00) };
		double[] antal = { 2, 1 };
		int size = p1.getOrdinationer().size();

		LocalDate start = LocalDate.of(2021, 02, 01);
		DagligSkaev ds = controller.opretDagligSkaevOrdination(start, LocalDate.of(2021, 2, 10), p1, l1, lt, antal);

		assertEquals(ds, p1.getOrdinationer().get(size));
	}

	@Test(expected = IllegalArgumentException.class)
	public void opretDagligskaevOrdinationTC2() {
		LocalTime[] lt = { LocalTime.of(8, 00), LocalTime.of(16, 00) };
		double[] antal = { 2, 1, 1 };
		int size = p1.getOrdinationer().size();

		LocalDate start = LocalDate.of(2021, 02, 01);
		DagligSkaev ds = controller.opretDagligSkaevOrdination(start, LocalDate.of(2021, 2, 1), p1, l1, lt, antal);

		assertEquals(ds, p1.getOrdinationer().get(size));
	}

	@Test(expected = IllegalArgumentException.class)
	public void opretDagligskaevOrdinationTC3() {
		LocalTime[] lt = { LocalTime.of(8, 00), LocalTime.of(16, 00) };
		double[] antal = { 2, 1 };
		int size = p1.getOrdinationer().size();

		LocalDate start = LocalDate.of(2021, 03, 01);
		DagligSkaev ds = controller.opretDagligSkaevOrdination(start, LocalDate.of(2021, 2, 1), p1, l1, lt, antal);

		assertEquals(ds, p1.getOrdinationer().get(size));
	}

	@Test(expected = IllegalArgumentException.class)
	public void opretDagligskaevOrdinationTC4() {
		LocalTime[] lt = { LocalTime.of(8, 00), LocalTime.of(16, 00) };
		double[] antal = { 2, 1 };
		int size = p1.getOrdinationer().size();

		LocalDate start = LocalDate.of(2021, 02, 01);
		DagligSkaev ds = controller.opretDagligSkaevOrdination(start, LocalDate.of(2021, 2, 1), p1, l1, lt, antal);

		assertEquals(ds, p1.getOrdinationer().get(size));
	}

	// -------------ordinationPNAnvendt()

	@Test
	public void ordinationPNAnvendtTC1() {
		LocalDate anvend = LocalDate.of(2021, 03, 05);

		PN pn = new PN(LocalDate.of(2021, 03, 01), LocalDate.of(2021, 03, 10), l1, 2.0);
		controller.ordinationPNAnvendt(pn, anvend);

		assertEquals(anvend, pn.getGivenDates().get(0));
	}

	@Test(expected = IllegalArgumentException.class)
	public void ordinationPNAnvendtTC2() {
		LocalDate anvend = LocalDate.of(2021, 02, 01);

		PN pn = new PN(LocalDate.of(2021, 03, 01), LocalDate.of(2021, 03, 10), l1, 2.0);
		controller.ordinationPNAnvendt(pn, anvend);

		assertEquals(anvend, pn.getGivenDates().get(0));
	}

	@Test(expected = IllegalArgumentException.class)
	public void ordinationPNAnvendtTC3() {
		LocalDate anvend = LocalDate.of(2021, 04, 01);

		PN pn = new PN(LocalDate.of(2021, 03, 01), LocalDate.of(2021, 03, 10), l1, 2.0);
		controller.ordinationPNAnvendt(pn, anvend);

		assertEquals(anvend, pn.getGivenDates().get(0));
	}

	// ------------anbefaletDosisprDoegn()

	@Test
	public void anbefaletDosisprDoegnTC1() {
		assertEquals(2, controller.anbefaletDosisPrDoegn(p2, l1), 0.001);
	}

	@Test
	public void anbefaletDosisprDoegnTC2() {
		assertEquals(12.48, controller.anbefaletDosisPrDoegn(p3, l1), 0.001);
	}

	@Test
	public void anbefaletDosisprDoegnTC3() {
		assertEquals(20.8, controller.anbefaletDosisPrDoegn(p4, l1), 0.001);
	}

	// --------------antalOrdinationerPrVægtPrLægemiddel
	@Test
	public void antalOrdinationerPrVægtPrLægemiddelTC1() {
		PN pn = new PN(LocalDate.of(2021, 03, 01), LocalDate.of(2021, 03, 10), l1, 2.0);
		p2.addOrdination(pn);
		p5.addOrdination(pn);
		p4.addOrdination(pn);

		assertEquals(2, controller.antalOrdinationerPrVægtPrLægemiddel(10, 80, l1), 0.001);
	}

	@Test
	public void antalOrdinationerPrVægtPrLægemiddelTC2() {
		PN pn = new PN(LocalDate.of(2021, 03, 01), LocalDate.of(2021, 03, 10), l1, 2.0);
		p2.addOrdination(pn);
		p5.addOrdination(pn);
		p4.addOrdination(pn);

		assertEquals(1, controller.antalOrdinationerPrVægtPrLægemiddel(50, 100, l1), 0.001);
	}

	@Test
	public void antalOrdinationerPrVægtPrLægemiddelTC3() {
		PN pn = new PN(LocalDate.of(2021, 03, 01), LocalDate.of(2021, 03, 10), l1, 2.0);
		p2.addOrdination(pn);
		p5.addOrdination(pn);
		p4.addOrdination(pn);

		assertEquals(2, controller.antalOrdinationerPrVægtPrLægemiddel(60, 140, l1), 0.001);
	}

	@Test
	public void antalOrdinationerPrVægtPrLægemiddelTC4() {
		PN pn = new PN(LocalDate.of(2021, 03, 01), LocalDate.of(2021, 03, 10), l1, 2.0);
		p2.addOrdination(pn);
		p5.addOrdination(pn);
		p4.addOrdination(pn);

		assertEquals(0, controller.antalOrdinationerPrVægtPrLægemiddel(80, 100, l1), 0.001);
	}

	// ----------opretPatient

	@Test
	public void opretPatientTC1() {
		Patient pa1 = controller.opretPatient("140507-3203", "Emil", 20);

		assertTrue(controller.getAllPatienter().contains(pa1));

	}

	@Test
	public void opretPatientTC2() {
		Patient pa1 = controller.opretPatient("", null, -70);

		assertTrue(controller.getAllPatienter().contains(pa1));

	}

	@Test
	public void opretPatientTC3() {
		Patient pa1 = controller.opretPatient(null, "", 140);

		assertTrue(controller.getAllPatienter().contains(pa1));

	}

	@Test
	public void opretPatientTC4() {
		Patient pa1 = controller.opretPatient("140507-3203", null, 100);

		assertTrue(controller.getAllPatienter().contains(pa1));

	}

	// -----------opretLaegemiddel

	@Test
	public void opretLaegemiddelTC1() {
		Laegemiddel la1 = controller.opretLaegemiddel("panodil", 0.1, 1.0, 1.6, "styk");

		assertTrue(controller.getAllLaegemidler().contains(la1));

	}

	@Test
	public void opretLaegemiddelTC2() {
		Laegemiddel la1 = controller.opretLaegemiddel("asdasd", -1, 1.4, -70, "ml");

		assertTrue(controller.getAllLaegemidler().contains(la1));

	}

	@Test
	public void opretLaegemiddelTC3() {
		Laegemiddel la1 = controller.opretLaegemiddel("", 200, 1.4, 1.5, "bananer");

		assertTrue(controller.getAllLaegemidler().contains(la1));

	}

	@Test
	public void opretLaegemiddelTC4() {
		Laegemiddel la1 = controller.opretLaegemiddel(null, 3, 1.4, 100, null);

		assertTrue(controller.getAllLaegemidler().contains(la1));

	}

}
