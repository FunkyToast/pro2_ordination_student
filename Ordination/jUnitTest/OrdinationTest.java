package jUnitTest;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.Laegemiddel;

public class OrdinationTest {
	private Laegemiddel l1;
	private DagligFast basisData1;

	@Before
	public void setUp() throws Exception {
		l1 = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "styk");
		basisData1 = new DagligFast(LocalDate.of(2021, 03, 01), LocalDate.of(2021, 03, 10), l1, 1, 0, 2, 1);
	}

	// ------antalDage()

	@Test
	public void antalDageTC1() {
		assertEquals(10, basisData1.antalDage());
	}

}
