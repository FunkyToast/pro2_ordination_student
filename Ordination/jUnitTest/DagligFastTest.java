package jUnitTest;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.Laegemiddel;
import ordination.Patient;

public class DagligFastTest {
	private Patient p1;
	private Laegemiddel l1;
	private DagligFast basisData1;

	@Before
	public void setUp() throws Exception {
		p1 = new Patient("121256-0512", "Jane Jensen", 63.4);
		l1 = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "styk");
		basisData1 = new DagligFast(LocalDate.of(2021, 03, 01), LocalDate.of(2021, 03, 10), l1, 1, 0, 2, 1);
	}

	// ---------samletDosis()
	@Test
	public void samletDosisTC1() {
		assertEquals(4, basisData1.samletDosis(), 0.001);
	}

	// ---------doegnDosis()
	@Test
	public void doegnDosisTC1() {
		assertEquals(0.4, basisData1.doegnDosis(), 0.001);
	}

}
