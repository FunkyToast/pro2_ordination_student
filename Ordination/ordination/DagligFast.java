package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;

public class DagligFast extends Ordination {
	private Dosis[] doser = new Dosis[4];

	public DagligFast(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double morgenAntal,
			double middagAntal, double aftenAntal, double natAntal) {
		super(startDen, slutDen, laegemiddel);

		Dosis morgen = new Dosis(LocalTime.of(6, 30), morgenAntal);
		Dosis middag = new Dosis(LocalTime.of(12, 00), middagAntal);
		Dosis aften = new Dosis(LocalTime.of(6, 30), aftenAntal);
		Dosis nat = new Dosis(LocalTime.of(23, 30), natAntal);
		doser[0] = morgen;
		doser[1] = middag;
		doser[2] = aften;
		doser[3] = nat;
	}

	@Override
	public double samletDosis() {
		
		double result = 0;
		for (Dosis dosis : doser) {
			result += dosis.getAntal();
		}

		return result;

	}

	@Override
	public double doegnDosis() {
		return samletDosis() / antalDage();
	}

	@Override
	public String getType() {
		return "DagligFast";
	}

	public Dosis[] getDoser() {
		return Arrays.copyOf(doser, doser.length);
	}
}