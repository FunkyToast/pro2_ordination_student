package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	private ArrayList<Dosis> dosisList;

	public DagligSkaev(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel) {
		super(startDen, slutDen, laegemiddel);
		dosisList = new ArrayList<>();
	}

	public void opretDosis(LocalTime tid, double antal) {
		Dosis dosis = new Dosis(tid, antal);
		dosisList.add(dosis);
	}

	public ArrayList<Dosis> getDoser() {
		return new ArrayList<>(dosisList);
	}

	@Override
	public double samletDosis() {
		double result = 0;

		for (Dosis d : dosisList) {
			result += d.getAntal();
		}
		return result;
	}

	@Override
	public double doegnDosis() {
		return samletDosis() / antalDage();
	}

	@Override
	public String getType() {
		return "DagligSkaev";
	}
}
