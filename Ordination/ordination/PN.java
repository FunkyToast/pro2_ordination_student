package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {

	private double antalEnheder;
	private ArrayList<LocalDate> givenDates = new ArrayList<>();

	public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double antal) {
		super(startDen, slutDen, laegemiddel);
		this.antalEnheder = antal;

	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
	 * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		if ((givesDen.isAfter(super.getStartDen()) || givesDen.equals(super.getStartDen()))
				&& (givesDen.isBefore(super.getSlutDen()) || givesDen.equals(super.getSlutDen()))) {
			givenDates.add(givesDen);
			return true;
		} else {
			return false;
		}
	}

	public double doegnDosis() {
		if (givenDates.size() < 1) {
			return 0;
		} else {
			LocalDate first = givenDates.get(0);
			LocalDate last = givenDates.get(0);
			for (LocalDate givenDate : givenDates) {
				if (givenDate.isBefore(first)) {
					first = givenDate;
				}
				if (givenDate.isAfter(last)) {
					last = givenDate;
				}
			}
			return samletDosis() / (ChronoUnit.DAYS.between(first, last) + 1);
		}
	}

	public double samletDosis() {
		return givenDates.size() * antalEnheder;
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int getAntalGangeGivet() {
		return givenDates.size();
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	public ArrayList<LocalDate> getGivenDates() {
		return new ArrayList<>(givenDates);
	}

	@Override
	public String getType() {
		return "pro necesare";
	}

}
